const express = require("express");
const router = express.Router();

const { verifyToken } = require("../middlewares/verifyToken");

const { adminRegister, adminLogin } = require("../controllers/admin");
const {
  addInventory,
  getInventory,
  addItemToInventory,
  addInventoryCategory,
  getItemForCategory,
  assignItem,
  getAllDataInCategory,
  getDashboardData
} = require("../controllers/inventory");
const {
  getEmployees,
  addEmployee,
  editEmployee,
  deleteEmployee,
  rejectRequest,
  changePasswordEmployee,
} = require("../controllers/employee");
const {
  getSupplier,
  addSupplier,
  editSupplier,
  deleteSupplier,
} = require("../controllers/supplier");
const { employeeLogin } = require("../controllers/employeeAuth");
const {
  getEmpoyeeProfile,
  saveRequest,
} = require("../controllers/employeeSide");

// Admin Routes
// router.post("/admin-register", adminRegister);
router.post("/admin-login", adminLogin);

// InventoryList Routes
router.post("/add-inventory", verifyToken, addInventory);
router.get("/get-inventory", verifyToken, getInventory);
router.post("/add-inventory-category", verifyToken, addInventoryCategory);
router.post("/get-all-data-in-category", verifyToken, getAllDataInCategory);

//Supplier Routes
router.get("/get-supplier", verifyToken, getSupplier);
router.post("/add-supplier", verifyToken, addSupplier);
router.post("/edit-supplier", verifyToken, editSupplier);
router.post("/delete-supplier", verifyToken, deleteSupplier);

//Employee Routes
router.get("/get-employees", verifyToken, getEmployees);
router.post("/add-employee", verifyToken, addEmployee);
router.post("/edit-employee", verifyToken, editEmployee);
router.post("/delete-employee", verifyToken, deleteEmployee);
router.post("/change-password-employee", verifyToken, changePasswordEmployee);

//Employee Side Routes

router.post("/employee-login", employeeLogin);
router.get("/get-employee-profile", verifyToken, getEmpoyeeProfile);

//items
router.post("/add-item-to-inventory", verifyToken, addItemToInventory);
router.get("/item-for-category/:category", getItemForCategory);
router.post("/assign-item", verifyToken, assignItem);
//request
router.post("/save-request", verifyToken, saveRequest);
router.post("/reject-request", verifyToken, rejectRequest);
router.get('/get-dashboard-data',getDashboardData);
module.exports = router;