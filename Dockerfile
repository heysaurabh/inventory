#dockerfile for node express backend

FROM node:16.13.1-alpine

#create app-directory

WORKDIR /server

COPY package*.json ./ 

RUN npm install --silent

COPY . .

EXPOSE 3001

CMD ["npm", "start"]