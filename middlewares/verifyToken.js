const jwt = require("jsonwebtoken");

exports.verifyToken = (req, res, next) => {
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1];
    const payload = jwt.verify(bearerToken, process.env.SECRET_KEY);
    if (!payload) {
      res.status(403).send({ message: "Unauthorized Request" });
    }
    req.userId = payload.subject;
    req.isAdmin = payload.isAdmin;
    next();
  } else {
    res.status(403).send({ message: "Unauthorized Request" });
  }
};
