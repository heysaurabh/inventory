import styled from "styled-components";

export const HomeContainer = styled.div`
  display: flex;
  height: 100vh;
  justify-content: center;
  align-items: center;
`;

export const HomeDiv = styled.div`
  width: ${(props) => (props.left ? "50vw" : "50vw")};
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 2px solid black;
  font-size: 3rem;
`;
export const AuthContainer = styled.div`
  width: 80%;

  height: 60%;
`;
export const BtnWrapper = styled.div`
  display: flex;

  justify-content: space-around;
  align-items: center;
`;

export const Form = styled.form`
  display: flex;
  font-size: 2rem;
  flex-direction: column;
  height: 90%;
  width: 80%;
  margin: auto;
  justify-content: center;
  align-items: center;
  div {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 1rem;
  }
  input {
    outline: none;
    padding: 1rem;
  }
`;
