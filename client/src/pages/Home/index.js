import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import { Button } from "../../components/ButtonElement";
import InputField from "../../components/InputField";
import {
  AuthContainer,
  BtnWrapper,
  HomeContainer,
  HomeDiv,
  Form,
} from "./HomeElements";

import AdminAuthService from "../../services/AdminAuthService";
import EmployeeAuthService from "../../services/EmployeeAuthService";

const Home = () => {
  const [admin, setAdmin] = useState(false);
  const navigate = useNavigate();

  const [credentials, setCredentials] = useState({
    uid: "",
    password: "",
  });
  const [error, setError] = useState({
    uid: "",
    password: "",
  });
  const validate = () => {
    if (credentials.uid.length === 0 && !admin) {
      setError({ ...error, uid: "Please enter your id" });
      return false;
    }
    if (credentials.password.length === 0) {
      setError({ ...error, password: "Please enter your password" });
      return false;
    } else {
      setError({ ...error, uid: "", password: "" });
    }
    return true;
  };
  const handleChange = (e) => {
    setCredentials({
      ...credentials,
      [e.target.name]: e.target.value,
    });
    if (e.target.name === "uid" && e.target.value.length === 0) {
      setError({ ...error, uid: "Please enter your id" });
    } else if (e.target.name === "password" && e.target.value.length === 0) {
      setError({ ...error, password: "Please enter your password" });
    } else {
      setError({ ...error, uid: "", password: "" });
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    // console.log(credentials);
    if (!validate()) {
      return;
    }
    if (admin) {
      const response = await AdminAuthService.login(credentials.password);
      console.log(response);
      if (response.status === 200) {
        localStorage.setItem("token", response.token);
        navigate("/admin");
      } else {
        if(response.status === 401){
        setError({ ...error, password: "Invalid password" });
        }
      }
    } else {
      const response = await EmployeeAuthService.login(credentials);
       console.log(response.status);
      
      if (response.status === 200) {
        localStorage.setItem("token", response.token);
        navigate("/employee");
      } else {
        if(response.status===401){
        setError({ ...error, password: "Invalid password" });
        }else if(response.status==404){
          setError({ ...error, uid: "Invalid id" });
        }
      }
    }
  };
  return (
    <HomeContainer>
      <HomeDiv>inventory</HomeDiv>
      <HomeDiv>
        <AuthContainer>
          <BtnWrapper>
            <Button
              onClick={() => setAdmin(false)}
              style={
                !admin
                  ? {
                      backgroundColor: "#1976d2",
                      color: "white",
                    }
                  : {}
              }
            >
              Employee
            </Button>
            <Button
              onClick={() => setAdmin(true)}
              style={
                admin
                  ? {
                      backgroundColor: "#1976d2",
                      color: "white",
                    }
                  : {}
              }
            >
              Admin
            </Button>
          </BtnWrapper>
          <Form onSubmit={handleSubmit}>
            {!admin && (
              <InputField
                type="text"
                placeholder="Enter UID"
                label="Employee UID"
                name="uid"
                value={credentials.uid}
                onChange={handleChange}
              />
            )}
            {error.uid && !admin && <p>{error.uid}</p>}
            <InputField
              type="password"
              placeholder="Enter your password"
              label="Password"
              name="password"
              value={credentials.password}
              onChange={handleChange}
            />
            {error.password && <p>{error.password}</p>}
            <Button type="submit">Signin</Button>
          </Form>
        </AuthContainer>
      </HomeDiv>
    </HomeContainer>
  );
};

export default Home;
