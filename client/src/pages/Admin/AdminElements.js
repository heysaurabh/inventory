import styled from "styled-components";
import { Link } from "react-router-dom";

export const SideLink = styled(Link)`
  text-decoration: none;
`;
