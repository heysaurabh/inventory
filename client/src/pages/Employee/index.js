import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import {
  Container,
  Toolbar,
  Typography,
  Box,
  Card,
  CardContent,
  Divider,
  Select,
  InputLabel,
  MenuItem,
  FormControl,
  TextField,
  Button,
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@mui/material";
import LogoutIcon from "@mui/icons-material/Logout";
import { GET_EMPLOYEE_PROFILE_ASYNC } from "../../actions/employeeSide";
import { GET_INVENTORY_ASYNC } from "../../actions/inventory";
import AuthService from "../../services/AuthService";
const Employee = () => {
  const navigate = useNavigate();

  const dispatch = useDispatch();
  const employee = useSelector((state) => state.employeeSide);
  const categories = useSelector((state) => {
    let temp = [];
    state.inventory?.map((item) => {
      temp = [...temp, ...item.type.categories];
    });
    return temp;
  });
  const [form, setForm] = React.useState({
    category: "",
    message: "",
  });

  const handleSelectChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const handleSubmitRequest = (e) => {
    e.preventDefault();
    // console.log("inside handle submit request");
    dispatch({ type: "SAVE_REQUEST", payload: form });
    Swal.fire("Request sent");
  };
  const handleLogout = () => {
    AuthService.logout();
    navigate("/");
  };
  useEffect(() => {
    dispatch({ type: GET_EMPLOYEE_PROFILE_ASYNC });
    dispatch({ type: GET_INVENTORY_ASYNC });
  }, []);
  return (
    <>
      <Container>
        <Toolbar sx={{ backgroundColor: "#6610f2", color: "white" }}>
          <Typography variant="h5" noWrap component="div">
            Inventory
          </Typography>

          <button
            style={{ position: "absolute", right: "20px" }}
            className="btn btn-danger"
            onClick={handleLogout}
          >
            <LogoutIcon /> Logout
          </button>
        </Toolbar>
        <Box
          sx={{
            "& > :not(style)": { m: 2 },
            marginY: 3,
          }}
        >
          <Card>
            <CardContent>
              <h3>
                {employee.name} - {employee.uid}
              </h3>
              <Divider />
              <Box>
                <p>{employee.designation}</p>
                <p>Email: {employee.email}</p>
                <p>Mobile Number: {employee.phone}</p>
              </Box>
            </CardContent>
          </Card>
          <Card>
            <CardContent>
              <h3>Assigned Resources</h3>
              <Divider />
              <Box
                sx={{
                  "& > :not(style)": { m: 1 },
                  display: "flex",
                  flexWrap: "wrap",
                }}
              >
                {employee?.itemsIssued?.map((item) => {
                  return (
                    <Card
                      key={item._id}
                      sx={{
                        minWidth: "200px",
                        minHeight: 120,
                        "&:hover": {
                          backgroundColor: "#E8E8E8",
                        },
                      }}
                    >
                      <CardContent>
                        <h4>{item.category}</h4>
                        <Divider />

                        {Object.keys(item.details).map(function (key, index) {
                          return (
                            <p key={index}>
                              {key}: {item.details[key]}
                            </p>
                          );
                        })}
                      </CardContent>
                    </Card>
                  );
                })}
              </Box>
            </CardContent>
          </Card>

          <Card>
            <CardContent>
              <h3>Request for new Resource</h3>
              <Divider />
              <FormControl sx={{ m: 2, minWidth: 250 }}>
                <InputLabel id="demo-simple-select-helper-label">
                  Choose what you need
                </InputLabel>
                <Select
                  labelId="demo-simple-select-helper-label"
                  onChange={handleSelectChange}
                  name="category"
                  label="Choose what you need"
                  value={form.category}
                >
                  {categories?.map((item, i) => {
                    return (
                      <MenuItem key={i} value={item}>
                        {item}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
              <FormControl sx={{ m: 2, minWidth: 300 }}>
                <TextField
                  rows={4}
                  cols={50}
                  multiline
                  onChange={handleSelectChange}
                  label="Enter your message"
                  name="message"
                  value={form.message}
                />
              </FormControl>
              <FormControl sx={{ m: 2, minWidth: 150 }}>
                <Button
                  variant="contained"
                  size="large"
                  onClick={handleSubmitRequest}
                >
                  Send Request
                </Button>
              </FormControl>
            </CardContent>
          </Card>
          <Card>
            <CardContent>
              <h3>Request History</h3>
              <Divider />
              <TableContainer component={Paper} sx={{ marginTop: "10px" }}>
                <Table sx={{ minWidth: "100%" }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell></TableCell>
                      <TableCell sx={{ fontWeight: "bold" }}>
                        Resourse Name
                      </TableCell>
                      <TableCell sx={{ fontWeight: "bold" }}>Message</TableCell>
                      <TableCell sx={{ fontWeight: "bold" }}>
                        Requested Date
                      </TableCell>
                      <TableCell sx={{ fontWeight: "bold" }}>
                        Approved Date
                      </TableCell>
                      <TableCell sx={{ fontWeight: "bold" }}>Status</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {employee?.requests?.map((item, i) => {
                      return (
                        <TableRow
                          key={item._id}
                          hover
                          sx={{
                            "&:last-child td, &:last-child th": { border: 0 },
                          }}
                        >
                          <TableCell>{i + 1}</TableCell>
                          <TableCell>{item.itemRequested?.category}</TableCell>
                          <TableCell>{item.itemRequested?.message}</TableCell>
                          <TableCell>{new Date(item?.dateRequested).getDate()}/{new Date(item?.dateRequested).getMonth()+1}/{new Date(item?.dateRequested).getFullYear()}</TableCell>
                          <TableCell>{new Date(item?.dateApproved).getDate()}/{new Date(item?.dateApproved).getMonth()+1}/{new Date(item?.dateApproved).getFullYear()}</TableCell>
                          <TableCell>
                            {item.approved && (
                              <Typography
                                variant="button"
                                sx={{ color: "green" }}
                              >
                                Approved
                              </Typography>
                            )}
                            {!item.approved && item.rejected && (
                              <Typography
                                variant="button"
                                sx={{ color: "red" }}
                              >
                                Rejected
                              </Typography>
                            )}
                            {!item.approved && !item.rejected && (
                              <Typography
                                variant="button"
                                sx={{ color: "blue" }}
                              >
                                Pending
                              </Typography>
                            )}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            </CardContent>
          </Card>
        </Box>
      </Container>
    </>
  );
};

export default Employee;
