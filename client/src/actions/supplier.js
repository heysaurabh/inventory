export const GET_SUPPLIER_REQUESTED = "GET_SUPPLIER_REQUESTED";
export const GET_SUPPLIER_SUCCESS = "GET_SUPPLIER_SUCCESS";
export const GET_SUPPLIER_FAILED = "GET_SUPPLIER_FAILED";

export const ADD_SUPPLIER_REQUESTED = "ADD_SUPPLIER_REQUESTED";
export const ADD_SUPPLIER_SUCCESS = "ADD_SUPPLIER_SUCCESS";
export const ADD_SUPPLIER_FAILED = "ADD_SUPPLIER_FAILED";

export const EDIT_SUPPLIER_REQUESTED = "EDIT_SUPPLIER_REQUESTED";
export const EDIT_SUPPLIER_SUCCESS = "EDIT_SUPPLIER_SUCCESS";
export const EDIT_SUPPLIER_FAILED = "EDIT_SUPPLIER_FAILED";

export const DELETE_SUPPLIER_REQUESTED = "DELETE_SUPPLIER_REQUESTED";
export const DELETE_SUPPLIER_SUCCESS = "DELETE_SUPPLIER_SUCCESS";
export const DELETE_SUPPLIER_FAILED = "DELETE_SUPPLIER_FAILED";

export const getSupplier = () => {
  return {
    type: GET_SUPPLIER_REQUESTED,
  };
};

export const addSupplier = (supplier) => {
  return {
    type: ADD_SUPPLIER_REQUESTED,
    supplier,
  };
};

export const editSupplier = (editData) => {
  // console.log(editData);
  return {
    type: EDIT_SUPPLIER_REQUESTED,
    editData,
  };
};

export const deleteSupplier = (deleteId) => {
  return {
    type: DELETE_SUPPLIER_REQUESTED,
    deleteId,
  };
};
