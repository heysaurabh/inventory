export const ADD_INVENTORY = "ADD_INVENTORY";

export const DELETE_INVENTORY = "DELETE_INVENTORY";
export const ADD_INVENTORY_ASYNC = "ADD_INVENTORY_ASYNC";
export const GET_INVENTORY_ASYNC = "GET_INVENTORY_ASYNC";
export const GET_INVENTORY = "GET_INVENTORY";
export const DELETE_INVENTORY_ASYNC = "DELETE_INVENTORY_ASYNC";
export const addInventory = (inventory) => {
  return {
    type: ADD_INVENTORY,
    payload: inventory,
  };
};
export const deleteInventory = (inventory) => {
  return {
    type: DELETE_INVENTORY,
    payload: inventory,
  };
};
export const getInventory = (inventory) => {
  return {
    type: GET_INVENTORY,
    payload: inventory,
  };
};
