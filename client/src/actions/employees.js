export const GET_EMPLOYEE = "GET_EMPLOYEE";
export const GET_EMPLOYEE_ASYNC = "GET_EMPLOYEE_ASYNC";
export const ADD_EMPLOYEE = "ADD_EMPLOYEE";
export const ADD_EMPLOYEE_ASYNC = "ADD_EMPLOYEE_ASYNC";
export const EDIT_EMPLOYEE = "EDIT_EMPLOYEE";
export const EDIT_EMPLOYEE_ASYNC = "EDIT_EMPLOYEE_ASYNC";
export const DELETE_EMPLOYEE = "DELETE_EMPLOYEE";
export const DELETE_EMPLOYEE_ASYNC = "DELETE_EMPLOYEE_ASYNC";

export const getEmployee = (employee) => {
  return {
    type: GET_EMPLOYEE,
    employee,
  };
};

export const addEmployee = (employee) => {
  return {
    type: ADD_EMPLOYEE,
    employee,
  };
};

export const deleteEmployee = (employee) => {
  return {
    type: DELETE_EMPLOYEE,
    employee,
  };
};
