import axios from "axios";
import ProtectedRoute from "./protected-routes/ProtectedRoute";

import "./App.css";
import Home from "./pages/Home/index";
import Admin from "./pages/Admin/index";
import Employee from "./pages/Employee";
import View from "./components/View";
import Employees from "./components/Employees";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import NotFound from "./components/NotFound";
import Dashboard from "./components/Dashboard";
import Requests from "./components/Requests";
import Inventory from "./components/Inventory";
import Supplier from "./components/Supplier";

axios.interceptors.request.use((request) => {
  request.headers.common.Authorization = `Bearer ${localStorage.getItem(
    "token"
  )}`;
  return request;
});

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route
            path="admin"
            element={
              <ProtectedRoute>
                <Admin />
              </ProtectedRoute>
            }
          >
            <Route index element={<Dashboard />} />
            <Route path="employees" element={<Employees />} />
            <Route path="requests" element={<Requests />} />
            <Route path="inventory" element={<Inventory />}></Route>
            <Route path="inventory/:id" element={<View />} />
            <Route path="supplier" element={<Supplier />} />
          </Route>
          <Route
            path="employee"
            element={
              <ProtectedRoute>
                <Employee />
              </ProtectedRoute>
            }
          />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
