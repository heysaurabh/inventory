import {
  GET_EMPLOYEE,
  ADD_EMPLOYEE,
  EDIT_EMPLOYEE,
  DELETE_EMPLOYEE,
} from "../actions/employees";

export default function employees(state = [], action) {
  switch (action.type) {
    case GET_EMPLOYEE:
      return [...action.employee];
    case ADD_EMPLOYEE:
      return [...state, action.employee];
    case EDIT_EMPLOYEE: {
      let index;
      let newState = state.filter((item, i) => {
        if (item._id === action.employee._id) {
          index = i;
        }
        return item._id !== action.employee._id;
      });
      newState.splice(index, 0, action.employee);
      return newState;
    }
    case DELETE_EMPLOYEE: {
      return state.filter((employee) => employee._id !== action.deleteId);
    }
    default:
      return state;
  }
}
