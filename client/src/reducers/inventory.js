import { ADD_INVENTORY, DELETE_INVENTORY } from "../actions/inventory";
import { GET_INVENTORY } from "../actions/inventory";
export default function inventory(state = [], action) {
  switch (action.type) {
    case ADD_INVENTORY:
      return [...state, action.payload];
    case GET_INVENTORY:
      return action.payload;
    case "ADD_INVENTORY_CATEGORY_SUCCESS":
      return []
    case DELETE_INVENTORY:
      return state.filter((inventory) => inventory.id !== action.payload.id);
    default:
      return state;
  }
}
