
import { GET_EMPLOYEE_PROFILE } from "../actions/employeeSide";

export default function employeeSide(state = {}, action) {
  switch (action.type) {
    case GET_EMPLOYEE_PROFILE:
      return action.employee;
    case "SAVE_REQUEST_SUCCESS":
      return {
        ...state,
        requests:[...state.requests,action.request]
      }
    default:
      return state;
  }
}
