import { combineReducers } from "redux";
// import { routerReducer } from "react-router-redux";
// import { reducer as formReducer } from "redux-form";
// import { reducer as toastrReducer } from "react-redux-toastr";
// import { reducer as modalReducer } from "react-redux-modal";
// import { reducer as toastrReducer2 } from "react-redux-toastr";
import inventory from "./inventory";
import employees from "./employees";
import supplier from "./supplier";
import employeeSide from "./employeeSide";

const combinedReducers = combineReducers({
  inventory,
  employees,
  supplier,
  employeeSide,
});
export default combinedReducers;
