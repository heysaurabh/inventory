import {
  GET_SUPPLIER_REQUESTED,
  GET_SUPPLIER_SUCCESS,
  GET_SUPPLIER_FAILED,
  ADD_SUPPLIER_REQUESTED,
  ADD_SUPPLIER_SUCCESS,
  ADD_SUPPLIER_FAILED,
  EDIT_SUPPLIER_REQUESTED,
  EDIT_SUPPLIER_SUCCESS,
  EDIT_SUPPLIER_FAILED,
  DELETE_SUPPLIER_REQUESTED,
  DELETE_SUPPLIER_SUCCESS,
  DELETE_SUPPLIER_FAILED,
} from "../actions/supplier";

const initialState = {
  suppliers: [],
  loading: false,
  error: null,
};

export default function supplier(state = initialState, action) {
  switch (action.type) {
    case GET_SUPPLIER_REQUESTED:
      return { ...state, loading: true };
    case GET_SUPPLIER_SUCCESS:
      return { ...state, suppliers: action.suppliers, loading: false };
    case GET_SUPPLIER_FAILED:
      return { ...state, loading: false, error: action.message };
    case ADD_SUPPLIER_REQUESTED:
      return { ...state, loading: true };
    case ADD_SUPPLIER_SUCCESS:
      return {
        ...state,
        loading: false,
        suppliers: [...state.suppliers, action.new],
      };
    case ADD_SUPPLIER_FAILED:
      return { ...state, loading: false, error: action.message };
    case EDIT_SUPPLIER_REQUESTED:
      return { ...state, loading: true };
    case EDIT_SUPPLIER_SUCCESS: {
      let index;
      let newState = {
        ...state,
        loading: false,
        suppliers: state.suppliers.filter((item, i) => {
          if (item._id === action.supplier._id) {
            index = i;
          }
          return item._id !== action.supplier._id;
        }),
      };
      newState.suppliers.splice(index, 0, action.supplier);
      return newState;
    }
    case EDIT_SUPPLIER_FAILED:
      return { ...state, loading: false, error: action.message };
    case DELETE_SUPPLIER_REQUESTED:
      return { ...state, loading: true };
    case DELETE_SUPPLIER_SUCCESS:
      return {
        ...state,
        loading: false,
        suppliers: state.suppliers.filter((item) => {
          return item._id !== action.deleteId;
        }),
      };
    case DELETE_SUPPLIER_FAILED:
      return { ...state, loading: false, error: action.message };
    default:
      return state;
  }
}
