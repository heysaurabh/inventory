import styled from "styled-components";

export const Button = styled.button`
  background: ${(props) => (props.primary ? "palevioletred" : "white")};
  color: ${(props) => (props.primary ? "white" : "palevioletred")};
  font-size: ${(props) => (props.large ? "1.5rem" : ".5em")};
  padding: 0.25em 0.5em;
  border: 2px solid palevioletred;
  border-radius: 50%;
  cursor: pointer;
`;
