import React, { useEffect } from "react";
// import { Button } from '../ButtonElement'
import { Button, Box } from "@mui/material";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import { Paper } from "@mui/material";
import DialogTitle from "@mui/material/DialogTitle";
import MoreVertIcon from "@mui/icons-material/MoreVert";
// import { InputLabel, Select, MenuItem, Grid } from "@material-ui/core";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import Popover from "@mui/material/Popover";
import { Form, DialogContainer, DynamicDivs } from "./InventoryElements";
import { useDispatch, useSelector } from "react-redux";
import { useMemo } from "react";
import {
  ADD_INVENTORY_ASYNC,
  GET_INVENTORY_ASYNC,
} from "../../actions/inventory";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";
let wrapper = {
  width: "100%",
};

const Inventory = () => {
  const dispatch = useDispatch();
  const inventoryData = useSelector((state) => state.inventory);
  const [temp,setTemp] = React.useState(inventoryData);
  const t=useMemo(()=>inventoryData,
  [inventoryData])
  
  useEffect(() => {
    // console.log("inventoryData", inventoryData);
    dispatch({ type: GET_INVENTORY_ASYNC });
    // console.log("Inventory");
  }, []);

  const [open, setOpen] = React.useState(false);
  const [inventory, setInventory] = React.useState({
    name: "",
    fields: [{ name: "", type: "" }],
  });
  const addField = () => {
    setInventory({
      ...inventory,
      fields: [...inventory.fields, { name: "", type: "" }],
    });
  };
  const deleteField = (index) => {
    const newFields = [...inventory.fields];
    newFields.splice(index, 1);
    setInventory({
      ...inventory,
      fields: newFields,
    });
  };
  const handleChange = (event, index) => {
    const newFields = [...inventory.fields];
    newFields[index][event.target.name] = event.target.value;
    setInventory({
      ...inventory,
      fields: newFields,
    });
    console.log(inventory);
  };
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openPopover, setOpenPopover] = React.useState("");
  const handleClick = (event, id) => {
    setAnchorEl(event.currentTarget);
    setOpenPopover(id);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setOpenPopover("");
  };

  const handleDelete = (_id) => {
    handleClose();
    // console.log(_id);
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
  };

  // const openPopover = Boolean(anchorEl);
  return (
    <div style={wrapper}>
      <Box sx={{ textAlign: "right", marginBottom: 3 }}>
        <Button variant="outlined" onClick={() => setOpen(true)}>
          Create New Inventory
        </Button>
      </Box>
      <Dialog
        open={open}
        onClose={() => {
          setOpen(false);
        }}
      >
        <DialogContainer>
          <DialogTitle>Create Inventory</DialogTitle>
          <DialogContent>
            <Form>
              <TextField
                label="Inventory Name"
                variant="outlined"
                type="text"
                value={inventory.name}
                onChange={(e) => {
                  setInventory({ ...inventory, name: e.target.value });
                }}
                placeholder="inventory name"
              />
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: "10px",
                }}
              >
                {inventory.fields.map((Item, index) => {
                  return (
                    <DynamicDivs key={index}>
                      <TextField
                        label="Field Name"
                        variant="outlined"
                        value={Item.fieldname}
                        onChange={(e) => handleChange(e, index)}
                        type="text"
                        name="name"
                      />

                      <Select
                        // value={Item.fieldtype || "text"}
                        onChange={(e) => handleChange(e, index)}
                        label="Type"
                        name="type"
                      >
                        <MenuItem value="text">Text</MenuItem>
                        <MenuItem value="number">Number</MenuItem>
                        <MenuItem value="date">Date</MenuItem>
                      </Select>
                      <Button onClick={deleteField}>
                        <DeleteIcon />
                      </Button>
                    </DynamicDivs>
                  );
                })}
              </div>
              <Button onClick={addField}>
                <AddIcon /> Add Field
              </Button>
            </Form>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setOpen(false)}>Close</Button>
            <Button
              onClick={() => {
                dispatch({ type: ADD_INVENTORY_ASYNC, payload: inventory });
              }}
            >
              save
            </Button>
          </DialogActions>
        </DialogContainer>
      </Dialog>
      <Box
        sx={{
          "& > :not(style)": { m: 1 },
          display: "flex",
          flexWrap: "wrap",
        }}
      >
        {inventoryData.map((item, index) => {
          return (
            <Paper
              style={{
                width: "250px",
                height: "250px",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                position: "relative",
              }}
              sx={{
                "&:hover": {
                  backgroundColor: "#E8E8E8",
                },
              }}
              elevation={2}
              key={index}
            >
              <MoreVertIcon
                style={{
                  position: "absolute",
                  top: "2%",
                  right: "2%",
                  fontSize: "2rem",
                  cursor: "pointer",
                }}
                onClick={(e) => handleClick(e, item._id)}
              />
              <Popover
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
                open={Boolean(openPopover)}
              >
                <Button
                  onClick={() => {
                    handleDelete(openPopover);
                  }}
                >
                  delete
                </Button>
              </Popover>
              <h3>
                <Link
                  style={{ textDecoration: "none", padding: "50px" }}
                  to={`${item._id}`}
                >
                  {item.type.name}
                </Link>
              </h3>
            </Paper>
          );
        })}
      </Box>
    </div>
  );
};

export default Inventory;
