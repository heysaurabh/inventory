import styled from 'styled-components';

export const Form = styled.form`
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-items:center;
    width:100%;
    height:100%;
    padding:10px;
    
`
export const DialogContainer=styled.div`
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-items:center;
    min-height:50vh;
`
export const DynamicDivs=styled.div`
    display:flex;
    justify-content:space-between;
    margin-top:10px;
`
