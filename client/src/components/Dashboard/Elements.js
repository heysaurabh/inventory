import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
`;

export const CardContainer = styled.div`
  // border:2px solid;
  height: 80%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  text-align: center;
`;

export const Card = styled.div`
  width: 30%;
  display: flex;
  height: 50%;
  // border:2px solid;
  place-items: center;
  font-size: 2rem;
`;
