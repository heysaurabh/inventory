import React from "react";
import { Box, Paper, responsiveFontSizes } from "@mui/material";
import { useEffect, useState } from "react";
import axios from "axios";
function Dashboard() {
  const [dashboardData, setDashboardData] = useState({});
  useEffect(() => {
    (async () => {
      try {
        const response = await axios.get("api/get-dashboard-data");
        console.log(response.data, "test");
        setDashboardData(response.data);
      } catch (err) {
        console.log(err);
      }
    })();
  }, []);

  return (
    <>
      <Box
        sx={{
          "& > :not(style)": { m: 2 },
          display: "flex",
          flexWrap: "wrap",
        }}
      >
        {Object.entries(dashboardData).map(([key, value]) => {
          return (
            <Paper
              sx={{
                width: "30vw",
                height: "30vh",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                position: "relative",
                "&:hover": {
                  backgroundColor: "#E8E8E8",
                },
                wordWrap: "breakWord"
              }}
              elevation={2}
            >
              <h1>{key}</h1>
              <h3>{value}</h3>
            </Paper>
          );
        })}
      </Box>
    </>
  );
}

export default Dashboard;
