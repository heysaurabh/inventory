import React, { useState, useEffect } from "react";
import {
  Dialog,
  DialogTitle,
  TextField,
  Box,
  Button,
  Divider,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { EDIT_EMPLOYEE_ASYNC } from "../../actions/employees";
import EmployeeAuthService from "../../services/EmployeeAuthService";

const EditDialog = (props) => {
  const { onClose, open, editId } = props;
  const dispatch = useDispatch();
  const data = useSelector((state) => {
    return state.employees.filter((item) => item._id === editId)[0];
  });
  const [newEmployee, setNewEmployee] = useState({
    name: "",
    uid: "",
    phone: "",
    email: "",
    designation: "",
  });

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setNewEmployee({ ...newEmployee, [name]: value });
  };

  const handleSubmit = () => {
    // console.log(editId);
    dispatch({ type: EDIT_EMPLOYEE_ASYNC, editData: { editId, newEmployee } });
    onClose();
  };

  useEffect(() => {
    setNewEmployee({
      name: data?.name,
      uid: data?.uid,
      phone: data?.phone,
      email: data?.email,
      designation: data?.designation,
    });
  }, [data]);

  const [newPassword, setNewPassword] = useState("");
  const handlePasswordSubmit = async (e) => {
    e.preventDefault();
    console.log("New Password", newPassword, editId);
    const response = await EmployeeAuthService.changePasswordEmployee({
      editId,
      newPassword,
    });
    if (response.success) {
      // console.log(response);
      onClose();
      setNewPassword("");
    }
  };
  return (
    <div>
      <Dialog onClose={onClose} open={open}>
        <DialogTitle>Edit Employee</DialogTitle>
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "30ch" },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            id="outlined-basic"
            label="Employee Name"
            variant="outlined"
            name="name"
            value={newEmployee.name}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="UID"
            variant="outlined"
            name="uid"
            value={newEmployee.uid}
            onChange={handleChange}
            required
          />

          <TextField
            id="outlined-basic"
            label="Mobile Number"
            variant="outlined"
            name="phone"
            value={newEmployee.phone}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Email Address"
            type="email"
            variant="outlined"
            name="email"
            value={newEmployee.email}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Designation"
            variant="outlined"
            name="designation"
            value={newEmployee.designation}
            onChange={handleChange}
            required
          />
          <Button onClick={handleSubmit} variant="outlined" size="large">
            Update
          </Button>
        </Box>
        <Divider />
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "30ch" },
          }}
          noValidate
          autoComplete="off"
        >
          <DialogTitle>Change Employee Password</DialogTitle>

          <TextField
            id="outlined-basic"
            label="New Password"
            variant="outlined"
            name="password"
            value={newPassword}
            onChange={(e) => {
              setNewPassword(e.target.value);
            }}
            required
          />
          <Button
            variant="outlined"
            size="large"
            onClick={handlePasswordSubmit}
          >
            Change Password
          </Button>
        </Box>
      </Dialog>
    </div>
  );
};

export default EditDialog;
