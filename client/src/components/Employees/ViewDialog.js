import React from "react";
import { useSelector } from "react-redux";
import {
  Dialog,
  DialogTitle,
  Box,
  Card,
  CardContent,
  Divider,
} from "@mui/material";

const ViewDialog = (props) => {
  const { onClose, open, viewId } = props;
  const employee = useSelector((state) => {
    return state.employees.filter((item) => item._id === viewId)[0];
  });

  return (
    <div>
      <Dialog onClose={onClose} open={open}>
        <DialogTitle>Assigned Resourses</DialogTitle>
        <Box
          sx={{
            "& > :not(style)": { m: 1 },
            display: "flex",
            flexWrap: "wrap",
            width: "50ch",
          }}
        >
          {employee?.itemsIssued.length === 0 && <h4>No resource assigned</h4>}
          {employee?.itemsIssued?.map((item) => {
            return (
              <Card
                key={item._id}
                sx={{
                  minWidth: "200px",
                  minHeight: 120,
                  "&:hover": {
                    backgroundColor: "#E8E8E8",
                  },
                }}
              >
                <CardContent>
                  <h4>{item.category}</h4>
                  <Divider />

                  {Object.keys(item.details).map(function (key, index) {
                    return (
                      <p key={index}>
                        {key}: {item.details[key]}
                      </p>
                    );
                  })}
                </CardContent>
              </Card>
            );
          })}
        </Box>
      </Dialog>
    </div>
  );
};

export default ViewDialog;
