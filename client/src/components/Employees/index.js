import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  GET_EMPLOYEE_ASYNC,
  DELETE_EMPLOYEE_ASYNC,
} from "../../actions/employees";
import { Box } from "@mui/system";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import VisibilityIcon from "@mui/icons-material/Visibility";
import Swal from "sweetalert2";
import {
  Button,
  Tooltip,
  TableContainer,
  Table,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@mui/material";
import AddDialog from "./AddDialog";
import EditDialog from "./EditDialog";
import ViewDialog from "./ViewDialog";

const Employees = () => {
  const dispatch = useDispatch();
  const employees = useSelector((state) => state.employees);

  // ADD Dialog
  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  // End ADD Dialog

  // View Dialog
  const [viewId, setViewId] = useState("");
  const [openView, setOpenView] = useState(false);

  const handleViewOpen = () => {
    setOpenView(true);
  };

  const handleViewClose = () => {
    setOpenView(false);
  };

  const handleView = (id) => {
    setViewId(id);
    handleViewOpen();
  };
  // END View Dialod

  // Edit Dialog

  const [editId, setEditId] = useState("");
  const [openEdit, setOpenEdit] = useState(false);

  const handleEditOpen = () => {
    setOpenEdit(true);
  };

  const handleEditClose = () => {
    setOpenEdit(false);
  };

  const handleEdit = (id) => {
    // console.log("Edit", id);
    setEditId(id);
    // console.log(editId);
    handleEditOpen();
  };

  // End Edit Dialog

  // Delete
  const handleDelete = (deleteId) => {
    // console.log("Delete ID: ", deleteId);
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Deleted!", "Employee has been deleted.", "success");
        dispatch({ type: DELETE_EMPLOYEE_ASYNC, deleteId });
      }
    });
  };
  // End Delete

  useEffect(() => {
    dispatch({ type: GET_EMPLOYEE_ASYNC });
  }, []);

  return (
    <>
      <Box sx={{ textAlign: "right", marginBottom: 3 }}>
        <Button variant="outlined" onClick={handleClickOpen}>
          Add New Employee
        </Button>
      </Box>
      <AddDialog open={open} onClose={handleClose} />
      <Box>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: "100%" }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell sx={{ fontWeight: "bold" }} align="right">
                  Employee Name
                </TableCell>
                <TableCell sx={{ fontWeight: "bold" }} align="right">
                  UID
                </TableCell>
                <TableCell sx={{ fontWeight: "bold" }} align="right">
                  Mobile Number
                </TableCell>
                <TableCell sx={{ fontWeight: "bold" }} align="right">
                  Email Address
                </TableCell>
                <TableCell sx={{ fontWeight: "bold" }} align="right">
                  Designation
                </TableCell>
                <TableCell
                  sx={{ fontWeight: "bold" }}
                  align="right"
                ></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {employees.map((employee, i) => {
                return (
                  <TableRow
                    key={i}
                    hover
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell>{i + 1}</TableCell>
                    <TableCell align="right">{employee?.name}</TableCell>
                    <TableCell align="right">{employee?.uid}</TableCell>
                    <TableCell align="right">{employee?.phone}</TableCell>
                    <TableCell align="right">{employee?.email}</TableCell>
                    <TableCell align="right">{employee?.designation}</TableCell>
                    <TableCell align="right">
                      <Tooltip title="View all assigned resources">
                        <Button
                          type="button"
                          onClick={() => handleView(employee?._id)}
                        >
                          <VisibilityIcon />
                        </Button>
                      </Tooltip>
                      <Tooltip title="Edit">
                        <Button
                          type="button"
                          onClick={() => handleEdit(employee?._id)}
                        >
                          <EditIcon />
                        </Button>
                      </Tooltip>

                      <Tooltip title="Delete">
                        <Button
                          type="button"
                          onClick={() => handleDelete(employee?._id)}
                        >
                          <DeleteIcon />
                        </Button>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
      <EditDialog open={openEdit} onClose={handleEditClose} editId={editId} />
      <ViewDialog open={openView} onClose={handleViewClose} viewId={viewId} />
    </>
  );
};

export default Employees;
