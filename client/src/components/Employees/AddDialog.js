import React, { useState } from "react";
import { Dialog, DialogTitle, TextField, Box, Button } from "@mui/material";
import { useDispatch } from "react-redux";
import { ADD_EMPLOYEE_ASYNC } from "../../actions/employees";

const AddDialog = (props) => {
  const { onClose, open } = props;
  const dispatch = useDispatch();
  const [newEmployee, setNewEmployee] = useState({
    name: "",
    uid: "",
    password: "",
    phone: "",
    email: "",
    designation: "",
  });

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setNewEmployee({ ...newEmployee, [name]: value });
  };

  const handleSubmit = () => {
    // console.log(newEmployee);
    dispatch({ type: ADD_EMPLOYEE_ASYNC, employee: newEmployee });
    onClose();
  };

  return (
    <div>
      <Dialog onClose={onClose} open={open}>
        <DialogTitle>Add Employee</DialogTitle>
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "30ch" },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            id="outlined-basic"
            label="Employee Name"
            variant="outlined"
            name="name"
            value={newEmployee.name}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="UID"
            variant="outlined"
            name="uid"
            value={newEmployee.uid}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Password"
            variant="outlined"
            name="password"
            value={newEmployee.password}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Mobile Number"
            variant="outlined"
            name="phone"
            value={newEmployee.phone}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Email Address"
            type="email"
            variant="outlined"
            name="email"
            value={newEmployee.email}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Designation"
            variant="outlined"
            name="designation"
            value={newEmployee.designation}
            onChange={handleChange}
            required
          />
          <Button onClick={handleSubmit} variant="outlined">
            Add
          </Button>
        </Box>
      </Dialog>
    </div>
  );
};

export default AddDialog;
