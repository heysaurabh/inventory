import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import socket from "socket.io-client";
import {
  Button,
  TextField,
  Box,
  Toolbar,
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Typography,
} from "@mui/material";
import { Modal } from "react-bootstrap";
import { InventoryApi } from "../../services/api";

const View = () => {
  let objtemp = {};
  const dispatch = useDispatch();
  const { id } = useParams();
  const obj = useSelector(
    (state) => state.inventory?.filter((item) => item._id == id)[0]?.type
  );
  const fields = obj?.fields;
  fields?.map((field) => (objtemp[field?.name] = ""));
  const categories = obj?.categories;
  const [form, setForm] = React.useState({});
  const [modalShow, setModalShow] = React.useState(false);
  const [category, setCategory] = React.useState("");
  const [categoryName, setCategoryName] = React.useState("");
  const [data, setData] = useState([]);
  useEffect(async () => {
    //let objtemp = {};
    // fields?.map((field) => (objtemp[field.name] = ""));
    if (fields === undefined) {
      dispatch({ type: "GET_INVENTORY_ASYNC", payload: id });
    } else {
      let data = await InventoryApi.getAllDataInCategory({ categories });
      setForm(objtemp);
      setData(data.data);
    }
  }, [fields]);
  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const handleSelectChange = (e) => {
    setCategoryName(e.target.value);
  };
  const handleClick = (e) => {
    e.preventDefault();
    dispatch({
      type: "ADD_INVENTORY_CATEGORY",
      payload: { inventoryId: id, category },
    });
  };
  const submit = () => {
    dispatch({ type: "SAVE_ITEM", payload: { form, categoryName } });
  };
  return (
    <>
      <h1>{obj?.name}</h1>
      <Box
        sx={{
          "& > :not(style)": { m: 1 },
        }}
      >
        <TextField
          id="outlined-basic"
          label="enter new category"
          variant="outlined"
          value={category}
          onChange={(e) => setCategory(e.target.value)}
        />
        <Button size="large" variant="outlined" onClick={handleClick}>
          add category
        </Button>

        <Button
          variant="outlined"
          sx={{ float: "right" }}
          size="large"
          onClick={() => setModalShow(true)}
        >
          enter new item to inventory
        </Button>
      </Box>
      <Modal
        show={modalShow}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        style={{ position: "absolute", left: "10%" }}
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            enter new item to inventory
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {Object.entries(form || {})?.map(([key, val], i) => {
            return (
              <div key={i}>
                <p>{key}</p>
                <input name={key} value={val} onChange={handleChange} />
              </div>
            );
          })}
          <select onChange={handleSelectChange} name="category">
            <option index="true">enter the category</option>
            {categories?.map((item, i) => {
              return (
                <option key={i} value={item}>
                  {item}
                </option>
              );
            })}
          </select>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="outlined" onClick={submit}>
            save
          </Button>
          <Button onClick={() => setModalShow(false)}>Close</Button>
        </Modal.Footer>
      </Modal>
      <Toolbar></Toolbar>
      <h4>All Items</h4>
      <Box>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: "100%" }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell sx={{ fontWeight: "bold" }}>Category</TableCell>
                {fields?.map((field, i) => {
                  return (
                    <TableCell key={i} sx={{ fontWeight: "bold" }}>
                      {field.name}
                    </TableCell>
                  );
                })}
                <TableCell sx={{ fontWeight: "bold" }}>Availability</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((item, i) => (
                <TableRow
                  key={item._id}
                  hover
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell>{i + 1}</TableCell>
                  <TableCell>{item.category}</TableCell>
                  {Object.keys(item.details).map(function (key, index) {
                    return (
                      <TableCell key={index}>{item.details[key]}</TableCell>
                    );
                  })}
                  <TableCell>
                    {item.assigned ? (
                      <Typography variant="button" sx={{ color: "green" }}>
                        Assigned
                      </Typography>
                    ) : (
                      <Typography variant="button" sx={{ color: "blue" }}>
                        Available
                      </Typography>
                    )}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </>
  );
};

export default View;
