import React, { useState, useEffect } from "react";
import {
  Button,
  Paper,
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Tooltip,
} from "@mui/material";
import { Box } from "@mui/system";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import AddDialog from "./AddDialog";
import EditDialog from "./EditDialog";
import Swal from "sweetalert2";

import { useDispatch, useSelector } from "react-redux";
import { getSupplier, deleteSupplier } from "../../actions/supplier";

const Supplier = () => {
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [editId, setEditId] = useState("");

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleEditOpen = () => {
    setOpenEdit(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleEditClose = () => {
    setOpenEdit(false);
  };

  const dispatch = useDispatch();

  const { suppliers, loading, error } = useSelector((state) => state.supplier);

  useEffect(() => {
    dispatch(getSupplier());
    // console.log(suppliers);
  }, []);

  // Edit
  const handleEdit = (id) => {
    setEditId(id);
    handleEditOpen();
    // console.log(id);
  };

  const handleDelete = (deleteId) => {
    // console.log(deleteId);
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteSupplier(deleteId));
        Swal.fire("Deleted!", "Supplier has been deleted.", "success");
      }
    });
  };

  return (
    <>
      <Box sx={{ textAlign: "right", marginBottom: 3 }}>
        <Button variant="outlined" onClick={handleClickOpen}>
          Add New Supplier
        </Button>
      </Box>
      <AddDialog open={open} onClose={handleClose} />

      {/* {loading && <p>Loading...</p>}
      {error && <p>{error}</p>} */}

      <Box>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: "100%" }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell sx={{ fontWeight: "bold" }}>Supplier Name</TableCell>
                <TableCell sx={{ fontWeight: "bold" }} align="right">
                  Category
                </TableCell>
                <TableCell sx={{ fontWeight: "bold" }} align="right">
                  Mobile Number
                </TableCell>
                <TableCell sx={{ fontWeight: "bold" }} align="right">
                  Email Address
                </TableCell>
                <TableCell sx={{ fontWeight: "bold" }} align="right">
                  Address
                </TableCell>
                <TableCell
                  sx={{ fontWeight: "bold" }}
                  align="right"
                ></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {suppliers.map((supplier, i) => (
                <TableRow
                  key={i}
                  hover
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell>{i + 1}</TableCell>
                  <TableCell component="th" scope="row">
                    {supplier.name}
                  </TableCell>
                  <TableCell align="right">{supplier.category}</TableCell>
                  <TableCell align="right">{supplier.phone}</TableCell>
                  <TableCell align="right">{supplier.email}</TableCell>
                  <TableCell align="right">{supplier.address}</TableCell>
                  <TableCell align="right">
                    <Tooltip title="Edit">
                      <Button
                        type="button"
                        onClick={() => handleEdit(supplier._id)}
                      >
                        <EditIcon />
                      </Button>
                    </Tooltip>
                    <Tooltip title="Delete">
                      <Button
                        type="button"
                        onClick={() => handleDelete(supplier._id)}
                      >
                        <DeleteIcon />
                      </Button>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
      <EditDialog open={openEdit} onClose={handleEditClose} editId={editId} />
    </>
  );
};

export default Supplier;
