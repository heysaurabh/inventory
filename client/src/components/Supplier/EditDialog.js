import React, { useState, useEffect } from "react";
import { Dialog, DialogTitle, TextField, Box, Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { editSupplier } from "../../actions/supplier";

const EditDialog = (props) => {
  const { onClose, open, editId } = props;
  const dispatch = useDispatch();
  const data = useSelector((state) => {
    return state.supplier.suppliers.filter((item) => item._id === editId)[0];
  });
  const [newSupplier, setNewSupplier] = useState({
    name: "",
    category: "",
    phone: "",
    email: "",
    address: "",
  });

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setNewSupplier({ ...newSupplier, [name]: value });
  };

  const handleSubmit = () => {
    // console.log("Inside Edit", editId);
    dispatch(editSupplier({ editId, newSupplier }));
    onClose();
  };

  useEffect(() => {
    setNewSupplier({
      name: data?.name,
      category: data?.category,
      phone: data?.phone,
      email: data?.email,
      address: data?.address,
    });
  }, [data]);

  return (
    <div>
      <Dialog onClose={onClose} open={open}>
        <DialogTitle>Edit Supplier</DialogTitle>
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "30ch" },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            id="outlined-basic"
            label="Supplier Name"
            variant="outlined"
            name="name"
            value={newSupplier.name}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Category"
            variant="outlined"
            name="category"
            value={newSupplier.category}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Mobile Number"
            variant="outlined"
            name="phone"
            value={newSupplier.phone}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Email Address"
            type="email"
            variant="outlined"
            name="email"
            value={newSupplier.email}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Address"
            variant="outlined"
            name="address"
            value={newSupplier.address}
            onChange={handleChange}
            multiline
            required
          />
          <Button onClick={handleSubmit} variant="outlined">
            Update
          </Button>
        </Box>
      </Dialog>
    </div>
  );
};

export default EditDialog;
