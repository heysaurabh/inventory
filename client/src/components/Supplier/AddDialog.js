import React, { useState } from "react";
import { Dialog, DialogTitle, TextField, Box, Button } from "@mui/material";
import { useDispatch } from "react-redux";
import { addSupplier } from "../../actions/supplier";

const AddDialog = (props) => {
  const { onClose, open } = props;
  const dispatch = useDispatch();
  const [newSupplier, setNewSupplier] = useState({
    name: "",
    category: "",
    phone: "",
    email: "",
    address: "",
  });

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setNewSupplier({ ...newSupplier, [name]: value });
  };

  const handleSubmit = () => {
    // console.log(newSupplier);
    dispatch(addSupplier(newSupplier));
    onClose();
    // setNewSupplier({
    //   name: "",
    //   category: "",
    //   phone: "",
    //   email: "",
    //   address: "",
    // });
  };

  return (
    <div>
      <Dialog onClose={onClose} open={open}>
        <DialogTitle>Add Supplier</DialogTitle>
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "30ch" },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            id="outlined-basic"
            label="Supplier Name"
            variant="outlined"
            name="name"
            value={newSupplier.name}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Category"
            variant="outlined"
            name="category"
            value={newSupplier.category}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Mobile Number"
            variant="outlined"
            name="phone"
            value={newSupplier.phone}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Email Address"
            type="email"
            variant="outlined"
            name="email"
            value={newSupplier.email}
            onChange={handleChange}
            required
          />
          <TextField
            id="outlined-basic"
            label="Address"
            variant="outlined"
            name="address"
            value={newSupplier.address}
            onChange={handleChange}
            multiline
            required
          />
          <Button onClick={handleSubmit} variant="outlined">
            Add
          </Button>
        </Box>
      </Dialog>
    </div>
  );
};

export default AddDialog;
