import React, { useEffect, useState } from "react";
import { Box } from "@mui/system";
import { Button, Card, CardContent, Divider } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import CheckIcon from "@mui/icons-material/Check";
import employees from "../../reducers/employees";
import { useSelector, useDispatch } from "react-redux";
import { GET_EMPLOYEE_ASYNC } from "../../actions/employees";
import { RequestApi } from "../../services/api";
import { Modal, Table } from "react-bootstrap";
import Swal from "sweetalert2";
import { useContext } from "react";
import { AppContext } from "../../index.js"
const Requests = () => {
  const socket=useContext(AppContext)
  const dispatch = useDispatch();
  const [toggle, setToggle] = useState(false);
  const [togglesecond,setTogglesecond] = useState(false);
  let [items, setItems] = useState([]);
  const [show, setShow] = useState(false);
  const [requestId, setRequestId] = useState("");
  const [flagNorequest, setFlag] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const employees = useSelector((state) => state.employees);
  const handleAccept = async (empIndex, reqIndex) => {
    let temp = await RequestApi.getItemForCategory(
      employees[empIndex]?.requests[reqIndex]?.itemRequested.category
    );
    setRequestId(employees[empIndex].requests[reqIndex]._id);
    setItems(temp.data);
    handleShow();
    // dispatch({ type: GET_EMPLOYEE_ASYNC });
  };
  const handleReject = async (empIndex, reqIndex) => {
    let res = await RequestApi.rejectRequest(
      employees[empIndex].requests[reqIndex]._id
    );
    Swal.fire("Okay", "Request has been rejected", "success");
    console.log(res?.data);
    setTogglesecond(!togglesecond);
    // dispatch({ type: GET_EMPLOYEE_ASYNC });
  };
  const assignItem = async (item) => {
    await RequestApi.assignItem(item._id, requestId);
    Swal.fire("Okay", "item has been assigned!", "success");
    setToggle(!toggle);
    handleClose();
  };
  useEffect(() => {
    dispatch({ type: GET_EMPLOYEE_ASYNC });
    socket.on("test",(arg)=>console.log(arg))
  }, [toggle,togglesecond]);   // in order to re-render the component when the state changes
  return (
    <>
      <Box
        sx={{
          "& > :not(style)": { m: 1 },
        }}
      >
        <Divider />
        {employees.reduce((len, emp) => len + emp.requests.length, 0) === 0 && (
          <h1>no requests to show</h1>
        )}
        {employees.map((employee, i) => {
          if (employee?.requests.length === 0) return null;
          return (
            <Box
              sx={{
                "& > :not(style)": { m: 1 },
              }}
              key={i}
            >
              <p>Request from:</p>
              <h4> {employee?.name} </h4>
              <Box
                sx={{
                  "& > :not(style)": { m: 1 },
                  display: "flex",
                  flexWrap: "wrap",
                }}
              >
                {employee?.requests.map((request, index) => {
                  if (request?.approved) return null;
                  return (
                    <Card
                      key={index}
                      sx={{
                        minWidth: "300px",
                        minHeight: 120,
                        "&:hover": {
                          backgroundColor: "#E8E8E8",
                        },
                      }}
                    >
                      <CardContent>
                        <div
                          style={{
                            display: "grid",
                            float: "right",
                            gridGap: "15px",
                            marginLeft: 10,
                          }}
                        >
                          <Button
                            variant="contained"
                            color="success"
                            startIcon={<CheckIcon />}
                            onClick={() => handleAccept(i, index)}
                          >
                            Accept
                          </Button>
                          <Button
                            variant="contained"
                            color="error"
                            onClick={() => handleReject(i, index)}
                            startIcon={<CloseIcon />}
                          >
                            Reject
                          </Button>
                        </div>
                        <h4>{request?.itemRequested.category}</h4>
                        <p>{request?.itemRequested.message}</p>
                      </CardContent>
                    </Card>
                  );
                })}
              </Box>
              <Divider />
            </Box>
          );
        })}
      </Box>
      <Modal
        style={{
          position: "absolute",
          left: "10%",
        }}
        centered
        show={show}
        onHide={handleClose}
      >
        <Modal.Header closeButton>
          <Modal.Title>Choose from the list of available items</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {items?.length === 0 && <h2>no items in this category</h2>}
          {items.length > 0 && (
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  {Object.keys(items[0]?.details).map((property, index) => {
                    return <th key={index}>{property}</th>;
                  })}
                </tr>
              </thead>

              <tbody>
                {items?.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      {Object.values(item?.details).map((propValue, i) => {
                        return <td key={item}>{propValue}</td>;
                      })}
                      <td>
                        <Button
                          variant="outlined"
                          onClick={() => assignItem(item)}
                        >
                          assign
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Requests;
