import React from "react";
import PropTypes from "prop-types";

const InputField = ({ value, label, placeholder, type, onChange, name }) => {
  return (
    <>
      <div>
        {label && <label>{label}</label>}
        {type === "textarea" ? (
          <textarea
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            name={name}
          />
        ) : (
          <input
            type={type}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            name={name}
          />
        )}
      </div>
    </>
  );
};

InputField.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  name: PropTypes.string
};

InputField.defaultProps = {
  value: "",
  label: "",
  placeholder: "",
  type: "text",
  name: "",
};

export default InputField;
