import { Navigate } from "react-router-dom";
import AuthService from "../services/AuthService";

export default function ProtectedRoute({ children }) {
  return AuthService.isLoggedIn() ? children : <Navigate to="/" />;
}
