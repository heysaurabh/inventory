const AuthService = {
  isLoggedIn: () => {
    try {
      return !!localStorage.getItem("token");
    } catch (error) {
      return console.log(error);
    }
  },
  logout: () => {
    try {
      localStorage.removeItem("token");
    } catch (error) {
      return console.log(error);
    }
  },
};

export default AuthService;
