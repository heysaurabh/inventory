import axios from "axios";

const getURL = "/api/get-supplier";
const addURL = "/api/add-supplier";
const editURL = "/api/edit-supplier";
const deleteURL = "/api/delete-supplier";

export function getSupplier() {
  return axios
    .get(getURL, {
      "Content-Type": "application/json",
    })
    .then((response) => {
      //   console.log(response);
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
}

export function addSupplier(supplier) {
  // console.log("Hello I am here",supplier);
  return axios
    .post(addURL, supplier, {
      "Content-Type": "application/json",
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
}

export function editSupplier(editData) {
  return axios
    .post(editURL, editData, {
      "Content-Type": "application/json",
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
}

export function deleteSupplier(deleteId) {
  return axios
    .post(
      deleteURL,
      { deleteId },
      {
        "Content-Type": "application/json",
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
}
