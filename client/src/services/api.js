import axios from "axios";

export const InventoryApi = {
  addInventory: (data) => {
    console.log({ type: data });
    return axios.post("/api/add-inventory", { type: data });
  },
  addInventoryCategory: (data) => {
    return axios.post("/api/add-inventory-category", data);
  },
  getInventory: () => {
    return axios.get("/api/get-inventory");
  },
  deleteInventory: (id) => {
    return axios.post("/api/delete-inventory/" + id);
  },
  saveItem: (obj) => {
    return axios.post("/api/add-item-to-inventory", obj);
  },
  getAllDataInCategory: (obj) => {
    // console.log("inside api for get all data",obj,"after")
    return axios.post("/api/get-all-data-in-category", obj);
  },
};
export const EmployeeApi = {};

export const RequestApi = {
  getItemForCategory: (category) => {
    return axios.get(`/api/item-for-category/${category}`);
  },
  assignItem: (itemId, requestId) => {
    return axios.post("/api/assign-item", { itemId, requestId });
  },
  rejectRequest: (requestId) => {
    return axios.post("/api/reject-request", { requestId });
  },
};
