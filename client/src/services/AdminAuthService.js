import axios from "axios";

const AdminAuthService = {
  login: async (password) => {
    try {
      const response = await axios.post("/api/admin-login", { password });
      console.log(response);
      return response
    } catch (err) {
      return err.response  // if error is thrown , handle and return response from here
      
    }
  },
};

export default AdminAuthService;
