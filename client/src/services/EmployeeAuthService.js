import axios from "axios";

const changePasswordURL = "/api/change-password-employee";

const EmployeeAuthService = {
  login: async (credential) => {
    try {
      const response = await axios.post("/api/employee-login", credential);
      return response.data;
    } catch (err) {
      console.log(err);
      return err.response;
    }
  },
  changePasswordEmployee: async (passwordData) => {
    try {
      const response = await axios.post(changePasswordURL, passwordData);
      return response.data;
    } catch (err) {
      console.log(err);
    }
  },
};

export default EmployeeAuthService;
