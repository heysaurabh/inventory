import axios from "axios";

const getURL = "/api/get-employees";
const addURL = "/api/add-employee";
const editURL = "/api/edit-employee";
const deleteURL = "/api/delete-employee";

export function getEmployee() {
  return axios
    .get(getURL, {
      "Content-Type": "application/json",
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
}

export function addEmployee(employee) {
  return axios
    .post(addURL, employee, {
      "Content-Type": "application/json",
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
}

export function editEmployee(editData) {
  return axios
    .post(editURL, editData, {
      "Content-Type": "application/json",
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
}

export function deleteEmployee(deleteId) {
  return axios
    .post(
      deleteURL,
      { deleteId },
      {
        "Content-Type": "application/json",
      }
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
}
