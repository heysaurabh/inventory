import axios from "axios";

const getURL = "/api/get-employee-profile";

export function getEmployeeProfile() {
  return axios
    .get(getURL, {
      "Content-Type": "application/json",
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
}

export function sendRequest(obj){
  console.log("inside save request api",obj)
  return axios.post("/api/save-request",obj);
}