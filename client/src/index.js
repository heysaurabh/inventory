import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas";
import reducer from "./reducers/index.js";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { createContext } from "react";
import { composeWithDevTools } from "redux-devtools-extension";
const { io } = require("socket.io-client");
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  reducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);
sagaMiddleware.run(rootSaga);
const socket = io(window.location.origin);
socket.on("connect",()=>{
  console.log("connected",socket.id);
})
export const AppContext = createContext();
ReactDOM.render(
  <React.StrictMode>
    <AppContext.Provider value={socket}>
    <Provider store={store}>
      <App />
    </Provider>
    </AppContext.Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
