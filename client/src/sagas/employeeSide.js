import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  GET_EMPLOYEE_PROFILE,
  GET_EMPLOYEE_PROFILE_ASYNC,
} from "../actions/employeeSide";
import { getEmployeeProfile, sendRequest } from "../services/EmployeeSide";

function* getEmployee() {
  try {
    const data = yield call(getEmployeeProfile);
    yield put({ type: GET_EMPLOYEE_PROFILE, employee: data });
  } catch (e) {
    console.log(e);
  }
}

function* watchGetEmployee() {
  yield takeEvery(GET_EMPLOYEE_PROFILE_ASYNC, getEmployee);
}

function* saveRequest(action) {
  try {
    console.log("inside saga for save request")
    let res = yield call(sendRequest, action.payload);
    yield put ({type: "SAVE_REQUEST_SUCCESS", request:res.data})
  } catch (err) {
    console.log(err);
  }
}
function* watchSaveRequest() {
  yield takeEvery("SAVE_REQUEST", saveRequest);
}
export default function* employeeSideSaga() {
  yield all([watchGetEmployee(), watchSaveRequest()]);
}
