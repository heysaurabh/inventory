import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  GET_EMPLOYEE,
  GET_EMPLOYEE_ASYNC,
  ADD_EMPLOYEE,
  ADD_EMPLOYEE_ASYNC,
  EDIT_EMPLOYEE,
  EDIT_EMPLOYEE_ASYNC,
  DELETE_EMPLOYEE,
  DELETE_EMPLOYEE_ASYNC,
} from "../actions/employees";
import {
  getEmployee,
  addEmployee,
  editEmployee,
  deleteEmployee,
} from "../services/employee";

// GET SAGA

function* getAllEmployee() {
  try {
    const employee = yield call(getEmployee);
    yield put({ type: GET_EMPLOYEE, employee });
  } catch (e) {
    console.log(e);
  }
}

function* watchGetEmployee() {
  yield takeEvery(GET_EMPLOYEE_ASYNC, getAllEmployee);
}

//ADD SAGA

function* addOneEmployee(action) {
  try {
    // console.log("action", action);
    const employee = yield call(addEmployee, action.employee);
    yield put({ type: ADD_EMPLOYEE, employee });
  } catch (error) {
    console.log(error);
  }
}

function* watchAddEmployee() {
  yield takeEvery(ADD_EMPLOYEE_ASYNC, addOneEmployee);
}

// EDIT SAGA

function* editOneEmployee(action) {
  try {
    const employee = yield call(editEmployee, action.editData);
    yield put({ type: EDIT_EMPLOYEE, employee });
  } catch (error) {
    console.log(error);
  }
}

function* watchEditEmployee() {
  yield takeEvery(EDIT_EMPLOYEE_ASYNC, editOneEmployee);
}

// DELETE SAGA

function* deleteOneEmployee(action) {
  try {
    const { deleteId } = yield call(deleteEmployee, action.deleteId);
    yield put({ type: DELETE_EMPLOYEE, deleteId });
  } catch (error) {
    console.log(error);
  }
}

function* watchDeleteEmployee() {
  yield takeEvery(DELETE_EMPLOYEE_ASYNC, deleteOneEmployee);
}

//ALL
export default function* employeeSaga() {
  yield all([
    watchGetEmployee(),
    watchAddEmployee(),
    watchEditEmployee(),
    watchDeleteEmployee(),
  ]);
}
