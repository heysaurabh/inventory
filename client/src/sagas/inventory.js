import { put, takeEvery, all } from "redux-saga/effects";
import {
  ADD_INVENTORY_ASYNC,
  GET_INVENTORY_ASYNC,
  DELETE_INVENTORY_ASYNC,
} from "../actions/inventory";
import { InventoryApi } from "../services/api";
import { addInventory as addInventoryAction } from "../actions/inventory";
import { getInventory as getInventoryAction } from "../actions/inventory";

function* addInventory(action) {
  try {
    // console.log("action", action);
    const res = yield InventoryApi.addInventory(action.payload);
    // console.log("res", res);
    yield put(addInventoryAction(res.data));
  } catch (error) {
    console.log(error);
  }
}

function* watchAddInventory() {
  yield takeEvery(ADD_INVENTORY_ASYNC, addInventory);
}

function* getInventory() {
  try {
    const res = yield InventoryApi.getInventory();
    // console.log("res", res);
    yield put(getInventoryAction(res.data));
  } catch (error) {
    console.log(error);
  }
}
function* watchGetInventory() {
  try {
    // console.log("inside get inventory async");
    yield takeEvery(GET_INVENTORY_ASYNC, getInventory);
  } catch (error) {
    console.log(error);
  }
}

function* deleteInventory(action) {
  try {
    // console.log("action", action);
    yield InventoryApi.deleteInventory(action.payload._id);
    // console.log("res", res);
    yield put();
  } catch (error) {}
}
function* watchDeleteInventory() {
  try {
    yield takeEvery(DELETE_INVENTORY_ASYNC, deleteInventory);
  } catch (error) {
    console.log(error);
  }
}
function* addInventoryCategory(action) {
  try {
    console.log("inside add invenotry category saga", action.payload);
    const res = yield InventoryApi.addInventoryCategory(action.payload);
    console.log(res, "res");
    // yield put({type:"ADD_INVENTORY_CATEGORY_SUCCESS", payload:res.data});
    yield put({ type: GET_INVENTORY_ASYNC });
  } catch (error) {
    console.log(error);
  }
}
function* watchAddInventoryCategory() {
  try {
    yield takeEvery("ADD_INVENTORY_CATEGORY", addInventoryCategory);
  } catch (error) {
    console.log(error);
  }
}
function* saveItem(action) {
  try {
    console.log("inside save item saga");
    yield InventoryApi.saveItem(action.payload);
  } catch (err) {
    console.log(err);
  }
}
function* watchSaveItem() {
  try {
    yield takeEvery("SAVE_ITEM", saveItem);
  } catch (err) {
    console.log(err);
  }
}
export default function* saga2() {
  yield all([
    watchAddInventory(),
    watchSaveItem(),
    watchGetInventory(),
    watchDeleteInventory(),
    watchAddInventoryCategory(),
  ]);
}
