import {
  GET_SUPPLIER_REQUESTED,
  GET_SUPPLIER_SUCCESS,
  GET_SUPPLIER_FAILED,
  ADD_SUPPLIER_REQUESTED,
  ADD_SUPPLIER_SUCCESS,
  ADD_SUPPLIER_FAILED,
  EDIT_SUPPLIER_REQUESTED,
  EDIT_SUPPLIER_SUCCESS,
  EDIT_SUPPLIER_FAILED,
  DELETE_SUPPLIER_REQUESTED,
  DELETE_SUPPLIER_SUCCESS,
  DELETE_SUPPLIER_FAILED,
} from "../actions/supplier";

import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  getSupplier,
  addSupplier,
  editSupplier,
  deleteSupplier,
} from "../services/supplier";

// GET SAGA

function* fetchSuppliers() {
  try {
    const suppliers = yield call(getSupplier);
    // console.log("here", suppliers);
    yield put({ type: GET_SUPPLIER_SUCCESS, suppliers });
  } catch (e) {
    yield put({ type: GET_SUPPLIER_FAILED, message: e.message });
  }
}

function* getSupplierWatcher() {
  yield takeEvery(GET_SUPPLIER_REQUESTED, fetchSuppliers);
}

// ADD SAGA

function* addNewSupplier(action) {
  try {
    // console.log("SAGA", action.supplier);
    const supplier = yield call(addSupplier, action.supplier);
    // console.log(supplier);
    yield put({ type: ADD_SUPPLIER_SUCCESS, new: supplier });
  } catch (e) {
    yield put({ type: ADD_SUPPLIER_FAILED, message: e.message });
  }
}

function* addSupplierWatcher() {
  yield takeEvery(ADD_SUPPLIER_REQUESTED, addNewSupplier);
}

// EDIT SAGA

function* editOneSupplier(action) {
  try {
    const supplier = yield call(editSupplier, action.editData);
    // console.log("SAGA", supplier);
    yield put({ type: EDIT_SUPPLIER_SUCCESS, supplier });
  } catch (e) {
    console.log(e);
    yield put({ type: EDIT_SUPPLIER_FAILED, message: e.message });
  }
}

function* editSupplierWatcher() {
  yield takeEvery(EDIT_SUPPLIER_REQUESTED, editOneSupplier);
}

// DELETE SAGA

function* deleteOneSupplier(action) {
  try {
    const { deleteId } = yield call(deleteSupplier, action.deleteId);
    yield put({ type: DELETE_SUPPLIER_SUCCESS, deleteId });
  } catch (e) {
    yield put({ type: DELETE_SUPPLIER_FAILED, message: e.message });
  }
}

function* deleteSupplierWatcher() {
  yield takeEvery(DELETE_SUPPLIER_REQUESTED, deleteOneSupplier);
}

// ALL SAGA

function* supplierSaga() {
  yield all([
    getSupplierWatcher(),
    addSupplierWatcher(),
    editSupplierWatcher(),
    deleteSupplierWatcher(),
  ]);
}

export default supplierSaga;
