import { fork } from "redux-saga/effects";
import employeeSaga from "./employees";
import saga2 from "./inventory";
import supplierSaga from "./supplier";
import employeeSideSaga from "./employeeSide";

export default function* rootSaga() {
  yield fork(employeeSaga);
  yield fork(saga2);
  yield fork(supplierSaga);
  yield fork(employeeSideSaga);
}
// all is used to run multiple sagas at once and is blocking
// takeEvery is used to run a saga when an action is dispatched
// fork is used to run a saga in the background
