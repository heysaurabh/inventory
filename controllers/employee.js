const bcrypt = require("bcrypt");

const Employee = require("../models/employee");
const Request = require("../models/requests");

exports.addEmployee = async function (req, res) {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  const { name, uid, password, phone, email, designation } = req.body;
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  var employee = new Employee({
    name,
    uid,
    password: hashedPassword,
    phone,
    email,
    designation,
  });
  employee.save(function (err, data) {
    if (err) {
      console.log(err);
      res.send(err);
    } else {
      data.password = "123456";
      res.send(data);
    }
  });
};

exports.getEmployees = function (req, res) {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  Employee.find(function (err, data) {
    if (err) {
      console.log(err);
      res.send(err);
    } else {
      data.map((emp) => {
        return (emp.password = "");
      });
      Promise.all(
        data.map(async (temp) => {
          await temp.populate({
            path: "itemsIssued requests",
          });
          temp.requests = temp.requests.filter(
            (t) => !t.approved && !t.rejected
          );
          return temp;
        })
      ).then((d) => {
         //console.log(d);
        res.send(d);
      });
    }
  });
};

exports.editEmployee = async function (req, res) {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  const { editId, newEmployee } = req.body;
  // const salt = await bcrypt.genSalt(10);
  // const hashedPassword = await bcrypt.hash(newEmployee.password, salt);
  Employee.findOneAndUpdate(
    { _id: editId },
    // { ...newEmployee, password: hashedPassword },
    newEmployee,
    { new: true },
    function (err, updatedEmployee) {
      if (err) {
        console.log(err);
        res.status(500).send(err);
      } else {
        updatedEmployee.password = "";
        res.status(200).send(updatedEmployee);
      }
    }
  );
};

exports.changePasswordEmployee = async function (req, res) {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  console.log(req.body);
  const { editId, newPassword } = req.body;

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(newPassword, salt);

  Employee.findOneAndUpdate(
    { _id: editId },
    { password: hashedPassword },
    function (err, updatedEmployee) {
      if (err) {
        console.log(err);
        res.status(500).send(err);
      } else {
        res
          .status(200)
          .send({ success: true, message: "Password updated successfully" });
      }
    }
  );
};

exports.deleteEmployee = function (req, res) {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  const { deleteId } = req.body;
  // console.log(deleteId);
  Employee.findOneAndDelete({ _id: deleteId }, function (err, deletedEmployee) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      res.status(200).send({ deleteId: deletedEmployee._id });
    }
  });
};

exports.rejectRequest = async (req, res) => {
  try {
    let request = await Request.findOne({ _id: req.body.requestId });
    request.rejected = true;
    await request.save();
    // console.log(request);
    res.status(200).send({ msg: "request rejected" });
  } catch (err) {
    console.log(err);
    res.send(err);
  }
};
