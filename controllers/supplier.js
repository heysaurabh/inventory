const Supplier = require("../models/supplier");

exports.getSupplier = function (req, res) {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  Supplier.find({}, function (err, supplier) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      res.status(200).send(supplier);
    }
  });
};

exports.addSupplier = function (req, res) {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  const { name, category, phone, email, address } = req.body;
  const supplier = new Supplier({
    name,
    category,
    phone,
    email,
    address,
  });
  supplier.save(function (err, supplier) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      res.status(200).send(supplier);
    }
  });
};

exports.editSupplier = function (req, res) {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  const { editId, newSupplier } = req.body;
  Supplier.findOneAndUpdate(
    { _id: editId },
    newSupplier,
    { new: true },
    function (err, updatedSupplier) {
      if (err) {
        console.log(err);
        res.status(500).send(err);
      } else {
        res.status(200).send(updatedSupplier);
      }
    }
  );
};

exports.deleteSupplier = function (req, res) {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  const { deleteId } = req.body;
  // console.log(deleteId);
  Supplier.findOneAndDelete({ _id: deleteId }, function (err, deletedSupplier) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      res.status(200).send({ deleteId: deletedSupplier._id });
    }
  });
};
