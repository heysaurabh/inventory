const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const Admin = require("../models/admin");

exports.adminRegister = async function (req, res) {
  const { password } = req.body;
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);
  const admin = new Admin({
    password: hashedPassword,
  });
  console.log(admin);
  admin.save(function (err, admin) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      res.status(200).send({ message: "Admin registered successfully" });
    }
  });
};

exports.adminLogin = function (req, res) {
  const { password } = req.body;
  Admin.findOne({}, function (err, admin) {
    if (err) {
      res.send(err);
    }
    // console.log("admin", admin);
    const validPassword = bcrypt.compareSync(password, admin.password);
    if (validPassword) {
      const token = jwt.sign(
        { subject: admin._id, isAdmin: true },
        process.env.SECRET_KEY
      );
      res.status(200).send({ message: "Admin logged in successfully", token });
    } else {
      res.status(401).send({ message: "Invalid password" });
    }
  });
  //   console.log("adminLogin", admin);
};
