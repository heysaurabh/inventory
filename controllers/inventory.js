const { Inventory, Item } = require("../models/inventory");
const Employee=require('../models/employee')
const Request = require("../models/requests");
const Supplier=require('../models/supplier')
exports.addInventory = function (req, res) {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  // console.log(req.body);
  const { type } = req.body;
  const inventory = new Inventory({ type });
  inventory.save(function (err, inventory) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      // res.status(200).send({ message: "Inventory added successfully" });
      res.status(200).send(inventory);
    }
  });
};

exports.getInventory = async function (req, res) {
  try {
    const inventories = await Inventory.find({});
    // const temp = inventories.map((inventory) => {
    //   const categories = inventory.type.categories;
    //   categories.map(async (category) => {
    //     const items = await Item.find({ category });
    //     inventory[category] = items;
    //     return category;
    //   });
    //   return inventory;
    // });
    console.log("inside shit")
    res.status(200).send(inventories);
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.addInventoryCategory = async (req, res) => {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  const { inventoryId, category } = req.body;
  const inventory = await Inventory.findById(inventoryId);
  inventory.type.categories.push(category);
  inventory.save(function (err, inventory) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      res.status(200).send(inventory);
    }
  });
};

exports.addItemToInventory = async (req, res) => {
  if (!req.isAdmin) {
    return res.status(401).send({ message: "You are not an Admin!" });
  }
  try {
    const item = new Item({
      details: req.body.form,
      category: req.body.category,
    });
    await item.save();
    
  } catch (err) {
    console.log(err);
  }
};

exports.getItemForCategory=async (req,res)=>{
  try{
  const category=req.params.category
 let items= await Item.find({category,assigned:false});
  return res.status(200).send(items);
  }
  catch(err){
    console.log(err);
  }
}
exports.assignItem=async (req,res)=>{
  try{
    let request=await Request.findOne({_id:req.body.requestId});
    console.log(request);
    request.approved=true;
    request.dateApproved=Date.now();
    let itemObj=await Item.findOne({_id:req.body.itemId})
    request.issuedItem=itemObj;
    await request.save();
    itemObj.assigned=true;
    await itemObj.save();
    let employee=await Employee.findOne({_id:request.userId})
    console.log(employee);
     employee.itemsIssued.push(itemObj);
    await employee.save();
    res.status(200).send({msg:"item assigned to user"});
  }catch(err){
    console.log(err)
  }
}

exports.getAllDataInCategory=async (req,res)=>{
  try{
    let items=[];
    items=await Promise.all(req.body.categories.map(async (category)=>{
      return await Item.find({category});
    }))
    items=items.reduce((res,item)=>{
      res=[...res,...item];
      return res;
    },[])
    res.status(200).send(items);
  }catch(err){
    console.log(err)
    res.status(404).send({msg:"error"})
  }
}

exports.getDashboardData=async (req,res)=>{
  // inventory count, requests count, items count, items assigned count, employees count, suppliers count
  try{
    console.log("tsrwsrs")
    let inventoryCount=await Inventory.countDocuments({});
    let requestsCount=await Request.countDocuments({approved:false});
    let itemsCount=await Item.countDocuments({});
    let itemsAssignedCount=await Item.countDocuments({assigned:true});
    let employeesCount=await Employee.countDocuments({});
    let suppliersCount=await Supplier.countDocuments({});
    res.status(200).send({inventoryCount,requestsCount,itemsCount,itemsAssignedCount,employeesCount,suppliersCount});
  }catch(err){
    console.log(err)
    res.status(404).send({msg:"error"})
  }
}