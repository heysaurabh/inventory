const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const Employee = require("../models/employee");

exports.employeeLogin = function (req, res) {
  const { uid, password } = req.body;
  Employee.findOne({ uid }, function (err, employee) {
    if (err || employee===null) {
      return res.status(404).send("Employee not found");
    }
    console.log("employee", employee);
    const validPassword = bcrypt.compareSync(password, employee.password);
    if (validPassword) {
      const token = jwt.sign(
        { subject: employee._id, isAdmin: false },
        process.env.SECRET_KEY
      );
      res
        .status(200)
        .send({ message: "Employee logged in successfully", token });
    } else {
      res.status(401).send({ message: "Invalid password" });
    }
  });
};
