const Employee = require("../models/employee");
const Request = require("../models/requests");
exports.getEmpoyeeProfile = function (req, res) {
  if (req.isAdmin) {
    return res.status(401).send({ message: "You are not an Employee!" });
  }
  Employee.findOne({ _id: req.userId }, function (err, employee) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      delete employee._doc.password;
      employee.populate("requests itemsIssued", function (err, employee) {
        if (err) {
          console.log(err);
          res.status(500).send(err);
        } else {
          console.log(employee);
          res.status(200).send(employee);
        }
      });
    }
  });
};

exports.saveRequest = function (req, res) {
  if (req.isAdmin) {
    return res.status(401).send({ message: "You are not an Employee!" });
  }
  Employee.findOne({ _id: req.userId }, function (err, employee) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      const request = new Request({
        userId: req.userId,
        itemRequested: req.body,
      });
      request.save();
      employee.requests.push(request);
      employee.save();
      console.log(request);
      res.status(200).send(request);
    }
  });
};
