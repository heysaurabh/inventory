const mongoose = require("mongoose");

const db = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@cluster0.wapcc.mongodb.net/InventoryDB?retryWrites=true&w=majority`;

try {
  mongoose.connect(
    db,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => console.log("connected to db")
  );
} catch (error) {
  console.log(error);
}
// mongoose.connect(
//   db,
//   { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false },
//   () => console.log("connected to db")
// );

module.exports = mongoose;
