const express = require("express");
const { createServer } = require("http");
const { Server } = require("socket.io");
const cors=require('cors');
require("dotenv").config();
const path=require('path')
const PORT = process.env.PORT || 3001;

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: "*",
  },
  // cors: {
  //   origin: `https://inventoryapp101.herokuapp.com/`, // I copied the origin in the error message and pasted here
  //   methods: ["GET", "POST"],
  //   credentials: true
  // }
});
app.use(express.json());
app.use(cors())
const routes = require("./routes/api");
app.use(express.static(path.join(__dirname, './client/build')));
app.use("/api", routes);



app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname + "/client/build/index.html"));
});

io.on("connection", (socket) => {
  console.log(socket.id);
  socket.emit("test", "hello");
});

httpServer.listen(PORT, () => {
  console.log(`server running on ${PORT}`);
});
