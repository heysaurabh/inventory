const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const employeeSchema = new Schema({
  uid: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  designation: {
    type: String,
  },
  itemsIssued: {
    type: [{ type: Schema.Types.ObjectId, ref: "Item" }],
    default: [],
  },
  requests: {
    type: [{ type: Schema.Types.ObjectId, ref: "Request" }],
    default: [],
  },
});

module.exports = mongoose.model("Employee", employeeSchema, "employees");
