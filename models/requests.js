const mongoose = require("mongoose");
const Item = require("./inventory");
const Schema = mongoose.Schema;

const requestsSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  itemRequested: {
    type: Object,
    required: true,
  },
  dateRequested: {
    type: Date,
    default: Date.now,
  },
  dateApproved: {
    type: Date,
  },
  approved: {
    type: Boolean,
    default: false,
  },
  rejected:{
    type:Boolean,
    default:false
  },
  issuedItem: {
    type: { type: Schema.Types.ObjectId, ref: "Item" },
  },
});

module.exports = mongoose.model("Request", requestsSchema);
