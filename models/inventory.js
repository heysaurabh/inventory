const mongoose=require('mongoose');
const Schema = mongoose.Schema;

const fieldSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
});

const itemSchema = new Schema({
  details:{
    type:Object
  },
   dateAdded: {
    type: Date,
    default: Date.now,
   },
   category: {
    type: String,
    required:true
   },
   assigned:{
       type:Boolean,
         default:false
   },
}, { strict: false });

const typeSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  fields: {
    type: [fieldSchema],
    required: true,
  },
  categories: [String],
});

const inventorySchema = new Schema({
  type: {
    type: typeSchema,
    required: true,
  }
},{strict:false});

const Item= mongoose.model("Item", itemSchema);
const Inventory = mongoose.model("Inventory", inventorySchema);
module.exports = {Item,Inventory};
